/************************************************************************************************************
* Apex Class Name   : OpportunityTriggerHandler.cls
* Version           : 1.0 
* Created Date      : 06 MARCH 2014
* Function          : Handler class for Opportunity Object Trigger
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Mitch Hunt                  06/03/2014              Created Default Handler Class Template
* Ashleey Ashokkumar          16/09/2014              disabled oppty renewal functionality
************************************************************************************************************/

public with sharing class OpportunityTriggerHandler
{
    private boolean m_bIsExecuting = false;
    private integer iBatchSize = 0;
    
    public OpportunityTriggerHandler(boolean bIsExecuting, integer iSize)
    {
        m_bIsExecuting = bIsExecuting;
        iBatchSize = iSize;
    }
    
    OpportunityUtils Utils = new OpportunityUtils();
    
    // EXECUTE BEFORE INSERT LOGIC
    //
    public void OnBeforeInsert(Opportunity[] lstNewOpportunities)
    {
       
    }
    
    // EXECUTE AFTER INSERT LOGIC
    //
    public void OnAfterInsert(Opportunity[] lstNewOpportunities)
    {
        
    }
    
    // BEFORE UPDATE LOGIC
    //
    public void OnBeforeUpdate(Opportunity[] lstOldOpportunities, Opportunity[] lstUpdatedOpportunities, map<ID, Opportunity> mapIDOpportunity)
    {
            Utils.debug('beforeupdate');
    }
    
    // AFTER UPDATE LOGIC
    //
    public void OnAfterUpdate(Opportunity[] lstOldOpportunities, Opportunity[] lstUpdatedOpportunities, map<ID, Opportunity> mapIDOpportunity)
    {
            Utils.debug('afterupdate');
    }
    
    // BEFORE DELETE LOGIC
    //
    public void OnBeforeDelete(Opportunity[] lstOpportunitiesToDelete, map<ID, Opportunity> mapIDOpportunity)
    {
        
    }
    
    // AFTER DELETE LOGIC
    //
    public void OnAfterDelete(Opportunity[] lstDeletedOpportunities, map<ID, Opportunity> mapIDOpportunity)
    {
        
    }
    
    // AFTER UNDELETE LOGIC
    //
    public void OnUndelete(Opportunity[] lstRestoredOpportunities)
    {
        
    }
    
    public boolean bIsTriggerContext
    {
        get{ return m_bIsExecuting; }
        set{ m_bIsExecuting = value;}
    }
    
    public boolean bIsVisualforcePageContext
    {
        get{ return !bIsTriggerContext; }
    }
    
    public boolean bIsWebServiceContext
    {
        get{ return !bIsTriggerContext; }
    }
    
    public boolean bIsExecuteAnonymousContext
    {
        get{ return !bIsTriggerContext; }
    }

}