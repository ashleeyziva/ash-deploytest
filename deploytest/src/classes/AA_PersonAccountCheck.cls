global class AA_PersonAccountCheck {
    
    public static Set<Id> checkIfValidPersonAccount (Map<Id, Account> newMap) {
        Map<Id, Id> personContacts = new Map<Id, Id>();
        Set<Id> result = new Set<Id>();
        
        for (Contact c : [SELECT Id FROM Contact WHERE Id in: newMap.keySet()]) {
            // change to personcontactid
            personContacts.put(c.Id, c.Id);
        }
        
        for (Account a : newMap.values()) {
            if (personContacts.containsKey(a.Id) <> true) {
                result.add(a.Id);
            }
        }
        
        return result;
    }
}