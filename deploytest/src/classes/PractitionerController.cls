public class PractitionerController {
    @AuraEnabled
    public static List<Practitioner__c> getAll() {
        return [select id, name, Profile__c, Primary_Skills__c from Practitioner__c order by name];
    }
    
}