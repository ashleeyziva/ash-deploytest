trigger AA_AccountAfter on Account (after insert, after update) {
    for (Id i : AA_PersonAccountCheck.checkIfValidPersonAccount(trigger.newMap)) {
        trigger.newMap.get(i).addError('this record is not a valid person account');
    }
}