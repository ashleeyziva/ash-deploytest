trigger Opportunity on Opportunity (after delete, after insert, after undelete, after update, before delete, before insert, before update)
{
    OpportunityTriggerHandler Handler = new OpportunityTriggerHandler(false, Trigger.size);
    
    // Before Insert Logic
    //
    if(Trigger.isInsert && Trigger.isBefore)
    {
        Handler.OnBeforeInsert(Trigger.new);
    }
    // After Insert Logic
    //
    else if(Trigger.isInsert && Trigger.isAfter)
    {
        Handler.OnAfterInsert(Trigger.new);
    }
    // Before Update Logic
    //
    else if(Trigger.isUpdate && Trigger.isBefore)
    {
        if (!Handler.bIsTriggerContext)
        {
            Handler.bIsTriggerContext = true;
            Handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap);
        }
    }
    // After Update Logic
    //
    else if(Trigger.isUpdate && Trigger.isAfter)
    {
        if (!Handler.bIsTriggerContext)
        {
            Handler.bIsTriggerContext = true;
            Handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap);
            Handler.bIsTriggerContext = false;
        }
    }
    // Before Delete Logic
    //
    else if(Trigger.isDelete && Trigger.isBefore)
    {
        Handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
    }
    // After Delete Logic
    //
    else if(Trigger.isDelete && Trigger.isAfter)
    {
        Handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
    }
    
    // After Undelete Logic
    //
    else if(Trigger.isUnDelete)
    {
        Handler.OnUndelete(Trigger.new);
    }
}