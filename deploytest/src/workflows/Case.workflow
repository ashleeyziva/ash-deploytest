<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Milestone_Missed</fullName>
        <description>Notify Milestone Missed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Milestone_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_Violated_False</fullName>
        <field>Violated__c</field>
        <literalValue>0</literalValue>
        <name>Case Violated False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Violated_True</fullName>
        <field>Violated__c</field>
        <literalValue>1</literalValue>
        <name>Case Violated True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Target_Time_Updated</fullName>
        <field>Target_Time__c</field>
        <formula>NOW()</formula>
        <name>Target Time Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Violation</fullName>
        <active>true</active>
        <formula>Violated__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Violated_False</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Target_Time_Updated</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
